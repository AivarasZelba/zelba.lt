<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'zelba_aivaras');

/** MySQL database username */
define('DB_USER', 'zelba_aivaras');

/** MySQL database password */
define('DB_PASSWORD', '0Q66j2ek');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h75yHIOjekEGn3RsoiFFgrxuoiCctd5D3noNIA39s7mk34aTVcZKtMOrwymoyVkz');
define('SECURE_AUTH_KEY',  'CFwUYb4KQLJjJ9FyYLd0Xi7ul0tiem8vQmi5mMML5kMYPphHGAmOkW1rzK7xps2Y');
define('LOGGED_IN_KEY',    'OT5RXjahoSXETC7cAm3j3KHRiN8ixVHkGsoexIU6j2qj1Qh1BbRn3MUU1sWZ1ssA');
define('NONCE_KEY',        'njeUhITe8Q55D7eGpDopancV3C9cWwABV4ZvqpGSbkM9F9CBaT4ymzGlIAGhMyVy');
define('AUTH_SALT',        'K5x6yKYrgx9auGNBHSOzf5l46tvMEo9es8fjntzJqjCVhYzkLttA4MW3dT0JfSqL');
define('SECURE_AUTH_SALT', 'ops8rZF5W29G63lPZt38qDCynrUeoMDnQbxKw3zs5lYMIZ7PifsM7LK2cY9u1m71');
define('LOGGED_IN_SALT',   'MkIPDfK9mMUce5OZbJ0PXSIzFGYvNF1gbQwLzEa1IxBOJ1vvQyeOmj57o3ZISFl0');
define('NONCE_SALT',       'hWPM8ita3moueIBChVa5bhA5pG2ZCy39ZwGRjo1xVRI1iUcuXaIIpe6RbcYaJYMv');

/**
 * Other customizations.
 *
 */
define('FS_METHOD','direct');
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'zelba_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
